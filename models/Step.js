var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Step Model
 * ==================
 */

var Step = new keystone.List('Step', {
	autokey: { from: 'name', path: 'key', unique: true },
});

Step.add({
	num: { type: String, required: false },
	name: { type: String, required: false },
	desc: { type: String, required: false },
	stories: { type: Types.Relationship, ref: 'Post', many: true },
});

Step.relationship({ ref: 'Post', path: 'steps' });

Step.register();
